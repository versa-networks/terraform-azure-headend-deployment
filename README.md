https://docs.versa-networks.com/Getting_Started/Deployment_and_Initial_Configuration/Headend_Deployment/Installation/Install_on_Azure#elm-main-content

# Install on Azure

1.  <dl class="mt-last-updated-container">

    <dt class="mt-last-updated-label">Last updated</dt>

    <dd><span class="mt-last-updated" data-timestamp="2021-10-21T15:48:34Z">Oct 21, 2021</span></dd>

    </dl>

</header>

<header class="mt-content-header"></header>

<section class="mt-content-container">

_![Versa-logo-release-icon.png](./img/Versa-logo-release-icon.png)For Releases 16.1R2 and later._

To install the Versa headend components on Microsoft Azure, you upload the Versa software images to the Azure portal and then you use Terraform templates to create virtual machines (VMs) for the Versa headened components. You obtain the Terraform template files the public repository hosted by Versa Networks, at [https://gitlab.com/versa-networks/te...end-deployment](https://gitlab.com/versa-networks/terraform-azure-headend-deployment "https://gitlab.com/versa-networks/terraform-azure-headend-deployment") (for headend deployments) and [https://gitlab.com/versa-networks/te...vnf-deployment](https://gitlab.com/versa-networks/terraform-azure-flexvnf-deployment "https://gitlab.com/versa-networks/terraform-azure-flexvnf-deployment") (for branch deployments).

<div mt-section-origin="Getting_Started/Deployment_and_Initial_Configuration/Headend_Deployment/Installation/Install_on_Azure" class="mt-section" id="section_1"><span id="Upload_Versa_Images_to_Azure_Portal"></span>

## Upload Versa Images to Azure Portal

Before you upload the Versa software images to the Azure portal, ensure that you have the following:

*   Valid Microsoft Azure subscription
*   Administrator or co-administrator credentials to the Azure management portal

To upload the Versa software images to the Azure portal:

1.  Log in to the Azure portal.  

    ![image2019-2-14_14-17-12.png](./img/image2019-2-14_14-17-12.png)
2.  In the left menu bar, click Storage Accounts.
3.  In the Storage Accounts menu bar, click the + Add icon. The Create Storage Account window displays.
    1.  In the Subscription field, select the subscription in which you want to create the storage account.
    2.  Select Resource Group or create new Resource Group where you want to create the storage account.
    3.  In the Storage Account Name field, enter a name for the storage account name.
    4.  In the Location field, select the location.
    5.  In the Account Kind field, select either Storage V2 (General Purpose V2) or Storage (General Purpose V1). Do not select BlobStorage.
    6.  Click Review + Create button to create a new storage account to store the Versa component images.  

        ![Storage_account.png](./img/Storage_account.png)  

        A confirmation window displays.  

        ![image2019-2-18_17-2-17.png](./img/image2019-2-18_17-2-17.png)
4.  In the Storage Accounts menu bar, click the newly created storage account. The storage account's window displays.
5.  Generate an SAS token:
    1.  In the storage account's menu bar, click the Shared Access Signature option.  

        ![sas-main.png](./img/sas-main.png)
    2.  In the Allowed Resource Types field, select all options.
    3.  In the Start Date field, select the date that is one day before the current date.
    4.  In the End Date field, select the date that is two days after the current date, to avoid having the upload operation expire if it is being performed across different time zones.  

        ![sas-date.png](./img/sas-date.png)
    5.  Keep all other options at the default values.
    6.  Click Generate SAS and Connection String to generate the SAS token.
    7.  Copy the generated SAS token (the second option on the screen, as highlighted in the screenshot below) to the clipboard to avoid any copy-and-paste errors.  

        ![sas-copy-token.png](./img/sas-copy-token.png)  

6.  Select the storage account that you created in Step 3\. In the storage account's menu bar, select Containers. The Containers window displays.
    1.  Click the + Container icon to add a container in which to store the Versa images.
    2.  In the Name field, enter a name for the container.
    3.  In the Public Access Level field, select Private access to the container.
    4.  Click OK to create the container.  

        ![Blob_creation.png](./img/Blob_creation.png)
7.  The Container window displays. In the Container Properties menu, make a note of the container's URL, which is shown in the URL field.  

    ![image2019-2-14_15-38-22.png](./img/image2019-2-14_15-38-22.png)
8.  In the storage account's menu bar, select Firewalls and Virtual Networks. In the Allow Access From field, ensure that All Networks is selected.  

    ![azure-firewalls-virtual.networks.png](./img/azure-firewalls-virtual.networks.png)
9.  Provide Versa Customer Support with the SAS token generated in Step 5, the URL of the container obtained in Step 7, and the required software version to install Versa headend components. Versa using this information to transfer the Azure VHD image to the customer's storage account, which you use in the next step to create an image.
10.  After Versa has transferred the VHD into the customer's account, create the image from that VHD.
    1.  In the Azure portal's main menu, click the Images option.
    2.  In the Images menu bar, click the + Add icon. The Create Image window displays.
    3.  In the Name field, enter a name for the image.
    4.  In the Subscription field, select the subscription to which Versa transferred the Azure VHD images.
    5.  In the Resource Group field, select the resource group or create a new resource group.
    6.  In the Location field, select the region name where you want to create the image. Ensure that you use the same region where you created the storage account and where Versa transferred the images.
    7.  In the OS Disk field, click Linux.
    8.  In the VM Generation field, select Gen 1\. Do not select Gen 2.
    9.  In the Storage Blob field, click Browse.  

   ![azure-vhd-image.png](./img/azure-vhd-image.png)  

    10.  Navigate to Storage account > Containers.
        1.  Select the container to which Versa transferred the VHD images.
        2.  Select the VHD image and click Select.  

   ![BrowseImage.png](./img/BrowseImage.png)
    11.  Select the Account type as Standard HDD.
    12.  Click the Create button. The Images window lists the newly created image.

</div>

<div mt-section-origin="Getting_Started/Deployment_and_Initial_Configuration/Headend_Deployment/Installation/Install_on_Azure" class="mt-section" id="section_2"><span id="Create_VMs_for_Versa_Headend_Components"></span>

## Create VMs for Versa Headend Components

This section describes how to use Terraform templates to automatically create VMs for Versa headend components.

Before you begin:

*   Obtain the Terraform template files from Versa Networks Customer Support. When requesting the template, specify whether you are using a standalone or a redundant headend topology. The figures below illustrate the topologies created by the Terraform template.
*   Install Terraform on your system. For information, see the [Download Terraform](https://www.terraform.io/downloads.html "https://www.terraform.io/downloads.html") article on the Terraform website.
*   Set up Terraform access to Azure to enable Terraform to provision resources into Azure. For information, see the [Microsoft Azure](https://docs.microsoft.com/en-us/azure/developer/terraform/ "https://docs.microsoft.com/en-us/azure/developer/terraform/") articles on the Microsoft website.
*   Obtain subscription ID, client ID, client secret, and tenant ID for logging in to Terraform.
*   Obtain Terraform Contributor-level user permission so that you can run the template.
*   Obtain images for the Versa headend components—Versa Director, Versa Analytics, and Versa Controller—from Versa Networks Customer Support in .vhd format and create images using those .VHD files.

The following figure illustrates the standalone headend topology created by the Terraform template. The Terraform template provisions resource groups, networks, and Versa headend instances, and it assigns networks to the instances. In this figure:

*   For MGMT_NETWORK, the management IP is assigned using this network, and the public IP addresses assigned are associated with the three ports to the three Versa headend components.
*   For Director-Controller-VAN_Network, the Director_SB, Controller_NB, and Analytics_NB IP addresses are assigned using this network. (SB is southbound, and NB is northbound.)
*   For Controller-Branch_Network, the Controller_SB IP address is assigned using this work. This IP address is used to connect to the branch. The public IP address is also assigned on the Controller WAN port.

![Topology_Versa_HE_Standalone_Azure-v2.png](./img/Topology_Versa_HE_Standalone_Azure-v2.png)

The following figure illustrates the redundant (high availability) headend topology created by the Terraform template.

![Topology_Versa_HE_HA_Azure-v2.png](./img/Topology_Versa_HE_HA_Azure-v2.png)

To create VMs using Terraform templates:

1.  Contact Versa Customer Support to obtain the Terraform template. When requesting the template, specify whether you are using a standalone or a redundant headend topology.
2.  When you receive the folder that contains the template files, save them to the local system on which Terraform is installed. The template folder contains the files needed to deploy the Versa headend VM resources on Azure. The table at the end of this section describes each of the files.
3.  If you want to make changes to any of the template files, for example, if you want to run another instance of Terraform, make a copy of the original folder and make your changes in the copy. It is recommended that you do not make any changes to the files in the original template folder.
4.  Open a console window on the local system where Terraform was installed. Go to the folder where all the required files are placed from the console window.
5.  Initialize Terraform. The initialization process downloads the Terraform plugins that are required to run the template.

<pre class="mt-indent-1">~$ terraform init
</pre>

![Terraform-init-Sample.jpg](./img/Terraform-init-Sample.jpg)

1.  Display all the resources provisioned as part of the template:

<pre class="mt-indent-1">~$ terraform plan
</pre>

![Terraform-plan-Sample.jpg](./img/Terraform-plan-Sample.jpg)

1.  Run the template to deploy all the VM resources on Azure:

<pre class="mt-indent-1">~$ terraform apply
</pre>

![Terraform-apply-Sample.jpg](./img/Terraform-apply-Sample.jpg)

The following table describes the contents of each Terrform template file and the actions performed by each file.

<table class="mt-responsive-table">

<thead>

<tr>

<th class="mt-column-width-15" scope="col">**Filename**</th>

<th class="mt-column-width-85" scope="col">**Description or Action**</th>

</tr>

</thead>

<tbody>

<tr>

<td class="mt-column-width-15" data-th="Filename" style="vertical-align:top;">main.tf</td>

<td class="mt-column-width-85" data-th="Description or Action" style="vertical-align:top;">

*   Provision one resource group, called Versa_HE. To change the resource group name, edit the terraform.tfvars file.
*   Provision the virtual network 10.234.0.0/16\. To change the IP prefix, edit the terraform.tfvars file.
*   Provision three /24 subnetworks. By default, 10.234.1.0/24, 10.234.2.0/24 & 10.234.3.0/24 subnets are used. To change the subnets, edit the terraform.tfvars file.
    *   10.234.1.0/24 subnet is for management of all the headend instances.
    *   10.234.2.0/24 subnet is the control network. This subnetwork is used for Director downstream (southbound) ports, Controller upstream (northbound) ports, and Analytics downstream (southbound ports.
    *   10.234.3.0/24 subnet is the WAN network. This subnetwork is used for Controller downstream (southbound) ports and branch connectivity.
*   Assign a public IP address on the management port of all management port instances.
*   Assign a static public IP address for the Controller WAN port.
*   Provision a route (a user-defined route) for the Controller address to use as a gateway for the IPSec overlay network address. This route is used to send Netconf traffic originating from the Director.
*   Provision a network security group and add all the firewall rules required to set up the headend.
*   Install a Director instance and run the cloud-init script to:
    *   Update the /etc/network/interface file.
    *   Update the /etc/hosts and /etc/hostname file.
    *   Add the SSH key for the Administrator user.
    *   Generate new certificates.
    *   Run the vnms-startup script in non-interactive mode.
*   Install a Controller instance and tun the runcloud-init script to:
    *   Update the /etc/network/interface file.
    *   Add the SSH key for the Admin user
*   Install a Analytics instance and run the cloud-init script to:
    *   Update the /etc/network/interface file.
    *   Update the /etc/hosts file.
    *   Add the ssh key for the Versa user
    *   Copy the certificate from the Director and install it on the Analytics node.

</td>

</tr>

<tr>

<td class="mt-column-width-15" data-th="Filename">var.tf</td>

<td class="mt-column-width-85" data-th="Description or Action">Provide definitions of all variables defined and used in the template. Do not make any changes to this file.</td>

</tr>

<tr>

<td class="mt-column-width-15" data-th="Filename" style="vertical-align:top;">terraform.tfvars</td>

<td class="mt-column-width-85" data-th="Description or Action" style="vertical-align:middle;">

User-defined input variables that are used to populate the Terraform templates. Edit this file to set or change the following variables:

*   analytics_vm_size—Instance type and size used to provision the Analytics instance. The default is Standard_DS3.
*   client_id—Client identifier information obtained as part of Setting up Terraform access.
*   client_secret—Client secret information obtained as part of setting up Terraform access.
*   controller_vm_size—Instance type and size used to provision the Controller instance. The default is Standard_DS3.
*   director_vm_size—Instance type and size used to provision the Director instance. The default is Standard_DS3.
*   hostname_director—Hostname of the Director instance. The default is versa-director.
*   image_analytics—Filename of the Analytics image.
*   image_controller—Filename of the Controller image.
*   image_director—Filename of the Director image.
*   location—Enter the string that identifies the headend location. For example, west-us or west-europe.
*   resource_group—Name of the resource group in which to place the resources for the headend. The default is Versa_HE.
*   ssh_key–SSH public key—This key is required to log in to the headend instances. To generate the SSH key, use the sshkey-gen or putty key generator command. You cannot generate keys within Azure.
*   tenant_id—Tenant identifier information obtained as part of Setting up Terraform access.

</td>

</tr>

<tr>

<td class="mt-column-width-15" data-th="Filename" style="vertical-align:middle;">output.tf</td>

<td class="mt-column-width-85" data-th="Description or Action">Output parameters, including management IP address, public IP address, CLI commands, and UI login information, for all instances. Do not make any changes to this file.</td>

</tr>

<tr>

<td class="mt-column-width-15" data-th="Filename">director.sh</td>

<td class="mt-column-width-85" data-th="Description or Action">Bash script that runs as part of cloud-init script on the Versa Director instance. Do not make any changes to this file.</td>

</tr>

<tr>

<td class="mt-column-width-15" data-th="Filename">controller.sh</td>

<td class="mt-column-width-85" data-th="Description or Action">Bash script that runs as part of cloud-init script on Versa Controller instance. Do not make any changes to this file.</td>

</tr>

<tr>

<td class="mt-column-width-15" data-th="Filename">van.sh</td>

<td class="mt-column-width-85" data-th="Description or Action">Bash script that runs as part of cloud-init script on Versa Analytics instance. Do not make any changes to this file.</td>

</tr>

</tbody>

</table>
