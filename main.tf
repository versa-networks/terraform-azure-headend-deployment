# Configure the Microsoft Azure Provider
provider "azurerm" {
    subscription_id = "${var.subscription_id}"
    client_id       = "${var.client_id}"
    client_secret   = "${var.client_secret}"
    tenant_id       = "${var.tenant_id}"
}

# Create a resource group in each region
resource "azurerm_resource_group" "versa_rg" {
    count    = "${length(var.location)}"
	name     = "${var.resource_group}-${1+count.index}"
    location = "${element(var.location, count.index)}"

    tags {
        environment = "VersaHeadEndHA"
    }
}

# Add template to use custom data for Director:
data "template_file" "user_data_dir" {
  count			= "${length(var.location)}"
  template = "${file("director-${1+count.index}.sh")}"
  
  vars {
    hostname_dir_master = "${var.hostname_director[0]}"
	hostname_dir_slave	= "${var.hostname_director[1]}"
    dir_master_mgmt_ip = "${azurerm_network_interface.director_nic_1.0.private_ip_address}"
	dir_slave_mgmt_ip = "${azurerm_network_interface.director_nic_1.1.private_ip_address}"
	hostname_van_1 = "${var.hostname_van[0]}"
	hostname_van_2	= "${var.hostname_van[1]}"
	van_1_mgmt_ip = "${azurerm_network_interface.van_nic_1.0.private_ip_address}"
	van_2_mgmt_ip = "${azurerm_network_interface.van_nic_1.1.private_ip_address}"
	sshkey = "${var.ssh_key}"
  }
}

# Add template to use custom data for Controller:
data "template_file" "user_data_ctrl" {
  template = "${file("controller.sh")}"
  
  vars {
	sshkey = "${var.ssh_key}"
    dir_master_mgmt_ip = "${azurerm_network_interface.director_nic_1.0.private_ip_address}"
	dir_slave_mgmt_ip = "${azurerm_network_interface.director_nic_1.1.private_ip_address}"
  }
}

# Add template to use custom data for Analytics:
data "template_file" "user_data_van" {
  count			= "${length(var.location)}"
  template = "${file("van-${1+count.index}.sh")}"
  
  vars {
    hostname_dir_master = "${var.hostname_director[0]}"
	hostname_dir_slave	= "${var.hostname_director[1]}"
    dir_master_mgmt_ip = "${azurerm_network_interface.director_nic_1.0.private_ip_address}"
	dir_slave_mgmt_ip = "${azurerm_network_interface.director_nic_1.1.private_ip_address}"
	hostname_van_1 = "${var.hostname_van[0]}"
	hostname_van_2	= "${var.hostname_van[1]}"
	van_1_mgmt_ip = "${azurerm_network_interface.van_nic_1.0.private_ip_address}"
	van_2_mgmt_ip = "${azurerm_network_interface.van_nic_1.1.private_ip_address}"
	sshkey = "${var.ssh_key}"
  }
}

# Create virtual network in each region
resource "azurerm_virtual_network" "versaNetwork" {
    count				= "${length(var.location)}"
    name				= "Versa_VNet-${1+count.index}"
	location			= "${element(var.location, count.index)}"
	address_space		= ["${element(var.vpc_address_space, count.index)}"]
	resource_group_name	= "${element(azurerm_resource_group.versa_rg.*.name, count.index)}"

    tags {
        environment = "VersaHeadEndHA"
    }
}

# Create Route Tables for Director to Router traffic pass through
resource "azurerm_route_table" "versa_udr_1" {
    count				= "${length(var.location)}"
    name				= "VersaRouteTableDir-Router-${1+count.index}"
    location			= "${element(var.location, count.index)}"
    resource_group_name	= "${element(azurerm_resource_group.versa_rg.*.name, count.index)}"
}

# Add Route in Route Tables for Director to Router traffic pass through
resource "azurerm_route" "versa_route_1" {
    count					= "${length(var.location)}"
    name					= "VersaRouteDir-Router-${1+count.index}"
    resource_group_name		= "${element(azurerm_resource_group.versa_rg.*.name, count.index)}"
	route_table_name		= "${element(azurerm_route_table.versa_udr_1.*.name, count.index)}"
    address_prefix			= "0.0.0.0/0"
    next_hop_type			= "VirtualAppliance"
    next_hop_in_ip_address	= "${element(azurerm_network_interface.router_nic_2.*.private_ip_address, count.index)}"
}

# Create Route Tables for Router to Controller traffic pass through
resource "azurerm_route_table" "versa_udr_2" {
    count				= "${length(var.location)}"
    name				= "VersaRouteTableRouter-Ctrl-${1+count.index}"
    location			= "${element(var.location, count.index)}"
    resource_group_name	= "${element(azurerm_resource_group.versa_rg.*.name, count.index)}"
}

# Add Route in Route Tables for Router to Controller traffic pass through
resource "azurerm_route" "versa_route_2" {
    count					= "${length(var.location)}"
    name					= "VersaRouteRouter-Ctrl-${1+count.index}"
    resource_group_name		= "${element(azurerm_resource_group.versa_rg.*.name, count.index)}"
	route_table_name		= "${element(azurerm_route_table.versa_udr_2.*.name, count.index)}"
    address_prefix			= "0.0.0.0/0"
    next_hop_type			= "VirtualAppliance"
    next_hop_in_ip_address	= "${element(azurerm_network_interface.controller_nic_2.*.private_ip_address, count.index)}"
}

# Create Route Tables for Router to Router traffic pass through
resource "azurerm_route_table" "versa_udr_3" {
    count				= "${length(var.location)}"
    name				= "VersaRouteTableRouter-Router-${1+count.index}"
    location			= "${element(var.location, count.index)}"
    resource_group_name	= "${element(azurerm_resource_group.versa_rg.*.name, count.index)}"
}

# Add Route in Route Tables for Router to Router traffic pass through
resource "azurerm_route" "versa_route_3" {
    count					= "${length(var.location)}"
    name					= "VersaRouteRouter-Router-${1+count.index}"
    resource_group_name		= "${element(azurerm_resource_group.versa_rg.*.name, count.index)}"
	route_table_name		= "${element(azurerm_route_table.versa_udr_3.*.name, count.index)}"
    address_prefix			= "0.0.0.0/0"
    next_hop_type			= "VirtualAppliance"
    next_hop_in_ip_address	= "${element(azurerm_network_interface.router_nic_3.*.private_ip_address, count.index)}"
}

# Create Management Subnet in each region
resource "azurerm_subnet" "mgmt_subnet" {
    count					= "${length(var.location)}"
    name					= "MGMT-NET-${1+count.index}"
    resource_group_name		= "${element(azurerm_resource_group.versa_rg.*.name, count.index)}"
	virtual_network_name	= "${element(azurerm_virtual_network.versaNetwork.*.name, count.index)}"
	address_prefix			= "${cidrsubnet("${element(azurerm_virtual_network.versaNetwork.*.address_space[count.index], count.index)}","${var.newbits_subnet}",1)}"
}

# Create Traffic Subnet for Director, Router and Analytics
resource "azurerm_subnet" "dir_router_network_subnet" {
    count					= "${length(var.location)}"
    name					= "Director-Router-Network-${1+count.index}"
    resource_group_name		= "${element(azurerm_resource_group.versa_rg.*.name, count.index)}"
	virtual_network_name	= "${element(azurerm_virtual_network.versaNetwork.*.name, count.index)}"
	address_prefix			= "${cidrsubnet("${element(azurerm_virtual_network.versaNetwork.*.address_space[count.index], count.index)}","${var.newbits_subnet}",2)}"
	route_table_id			= "${element(azurerm_route_table.versa_udr_1.*.id, count.index)}"
}

# Create Traffic Subnet for Router and Router connectivity
resource "azurerm_subnet" "router_network_subnet" {
    count					= "${length(var.location)}"
    name					= "Router-Router-Network-${1+count.index}"
    resource_group_name		= "${element(azurerm_resource_group.versa_rg.*.name, count.index)}"
	virtual_network_name	= "${element(azurerm_virtual_network.versaNetwork.*.name, count.index)}"
	address_prefix			= "${cidrsubnet("${element(azurerm_virtual_network.versaNetwork.*.address_space[count.index], count.index)}","${var.newbits_subnet}",3)}"
	route_table_id			= "${element(azurerm_route_table.versa_udr_3.*.id, count.index)}"
}

# Create Traffic Subnet for Router and Controller (Control Network)
resource "azurerm_subnet" "control_network_subnet" {
    count					= "${length(var.location)}"
    name					= "CONTROL-NET-${1+count.index}"
    resource_group_name		= "${element(azurerm_resource_group.versa_rg.*.name, count.index)}"
    virtual_network_name	= "${element(azurerm_virtual_network.versaNetwork.*.name, count.index)}"
    address_prefix			= "${cidrsubnet("${element(azurerm_virtual_network.versaNetwork.*.address_space[count.index], count.index)}","${var.newbits_subnet}",4)}"
    route_table_id			= "${element(azurerm_route_table.versa_udr_2.*.id, count.index)}"
}

# Create Traffic Subnet for Controller and Branch (WAN Network)
resource "azurerm_subnet" "wan_network_subnet" {
    count					= "${length(var.location)}"
    name					= "WAN-NET-${1+count.index}"
    resource_group_name		= "${element(azurerm_resource_group.versa_rg.*.name, count.index)}"
	virtual_network_name	= "${element(azurerm_virtual_network.versaNetwork.*.name, count.index)}"
	address_prefix			= "${cidrsubnet("${element(azurerm_virtual_network.versaNetwork.*.address_space[count.index], count.index)}","${var.newbits_subnet}",5)}"
}

# Create Traffic Subnet for Branch and Client (LAN Network)
resource "azurerm_subnet" "lan_network_subnet" {
    count					= "${length(var.location)}"
    name					= "LAN-NET-${1+count.index}"
    resource_group_name		= "${element(azurerm_resource_group.versa_rg.*.name, count.index)}"
	virtual_network_name	= "${element(azurerm_virtual_network.versaNetwork.*.name, count.index)}"
	address_prefix			= "${cidrsubnet("${element(azurerm_virtual_network.versaNetwork.*.address_space[count.index], count.index)}","${var.newbits_subnet}",6)}"
}

# Create Public IP for Directors
resource "azurerm_public_ip" "ip_dir" {
    count							= "${length(var.location)}"
    name							= "PublicIP_Director-${1+count.index}"
    location						= "${element(var.location, count.index)}"
    resource_group_name				= "${element(azurerm_resource_group.versa_rg.*.name, count.index)}"
    public_ip_address_allocation	= "dynamic"

    tags {
        environment = "VersaHeadEndHA"
    }
}

# Create Public IP for Routers
resource "azurerm_public_ip" "ip_router" {
    count							= "${length(var.location)}"
    name							= "PublicIP_Router-${1+count.index}"
    location						= "${element(var.location, count.index)}"
    resource_group_name				= "${element(azurerm_resource_group.versa_rg.*.name, count.index)}"
    public_ip_address_allocation	= "dynamic"

    tags {
        environment = "VersaHeadEndHA"
    }
}

# Create Public IP for Controllers
resource "azurerm_public_ip" "ip_ctrl" {
    count							= "${length(var.location)}"
    name							= "PublicIP_Controller-${1+count.index}"
    location						= "${element(var.location, count.index)}"
    resource_group_name				= "${element(azurerm_resource_group.versa_rg.*.name, count.index)}"
    public_ip_address_allocation	= "dynamic"

    tags {
        environment = "VersaHeadEndHA"
    }
}

# Create Public IP for Controllers WAN Interface
resource "azurerm_public_ip" "ip_ctrl_wan" {
    count							= "${length(var.location)}"
    name							= "PublicIP_Controller_WAN-${1+count.index}"
    location						= "${element(var.location, count.index)}"
    resource_group_name				= "${element(azurerm_resource_group.versa_rg.*.name, count.index)}"
    public_ip_address_allocation	= "static"

    tags {
        environment = "VersaHeadEndHA"
    }
}

# Create Public IP for Analytics
resource "azurerm_public_ip" "ip_van" {
    count							= "${length(var.location)}"
    name							= "PublicIP_VAN-${1+count.index}"
    location						= "${element(var.location, count.index)}"
    resource_group_name				= "${element(azurerm_resource_group.versa_rg.*.name, count.index)}"
    public_ip_address_allocation	= "dynamic"

    tags {
        environment = "VersaHeadEndHA"
    }
}


# Create Network Security Groups and rules
resource "azurerm_network_security_group" "versa_nsg" {
    count							= "${length(var.location)}"
    name							= "VersaNSG-${1+count.index}"
    location						= "${element(var.location, count.index)}"
    resource_group_name				= "${element(azurerm_resource_group.versa_rg.*.name, count.index)}"
	security_rule					= [
	  {
        name                       = "Versa_Security_Rule_TCP-${1+count.index}"
        priority                   = 151
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_ranges     = ["22", "9182-9183", "443", "8009", "9080", "2024", "4566", "4570", "8443", "5432", "9090", "4949", "6080", "20514", "80", "2022", "2812", "3000-3002", "4000", "4566", "4570", "5000", "7000-7001", "7199", "8008", "8080", "8983", "9042", "9160", "1234"]
        source_address_prefix      = "*"
        destination_address_prefix = "*"
      },
	  {
        name                       = "Versa_Security_Rule_UDP-${1+count.index}"
        priority                   = 201
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Udp"
        source_port_range          = "*"
        destination_port_ranges     = ["20514", "53", "123", "500", "4500", "4790"]
        source_address_prefix      = "*"
        destination_address_prefix = "*"
      },
	  {
        name                       = "Versa_Security_Rule_Outbound-${1+count.index}"
        priority                   = 251
        direction                  = "Outbound"
        access                     = "Allow"
        protocol                   = "*"
        source_port_range          = "*"
        destination_port_range     = "*"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
      },
	  {
        name                       = "Versa_Security_Rule_ESP-${1+count.index}"
        priority                   = 301
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "*"
		source_address_prefix      = "VirtualNetwork"
		destination_address_prefix = "*"
        source_port_range          = "*"
        destination_port_range     = "*"
      }
	]

    tags {
        environment = "VersaHeadEndHA"
    }
}

# Create Management network interface for Directors
resource "azurerm_network_interface" "director_nic_1" {
    count								= "${length(var.location)}"
    name								= "Director_NIC1-${1+count.index}"
    location							= "${element(var.location, count.index)}"
    resource_group_name					= "${element(azurerm_resource_group.versa_rg.*.name, count.index)}"
    network_security_group_id			= "${element(azurerm_network_security_group.versa_nsg.*.id, count.index)}"

    ip_configuration {
        name							= "Director_NIC1_Configuration-${1+count.index}"
		subnet_id						= "${element(azurerm_subnet.mgmt_subnet.*.id, count.index)}"
        private_ip_address_allocation	= "dynamic"
        public_ip_address_id			= "${element(azurerm_public_ip.ip_dir.*.id, count.index)}"
    }

    tags {
        environment = "VersaHeadEndHA"
    }
}

# Create Southbound network interface for Directors
resource "azurerm_network_interface" "director_nic_2" {
    count								= "${length(var.location)}"
    name								= "Director_NIC2-${1+count.index}"
    location							= "${element(var.location, count.index)}"
    resource_group_name					= "${element(azurerm_resource_group.versa_rg.*.name, count.index)}"
    network_security_group_id			= "${element(azurerm_network_security_group.versa_nsg.*.id, count.index)}"

    ip_configuration {
        name							= "Director_NIC2_Configuration-${1+count.index}"
		subnet_id						= "${element(azurerm_subnet.dir_router_network_subnet.*.id, count.index)}"
        private_ip_address_allocation	= "dynamic"
    }

    tags {
        environment = "VersaHeadEndHA"
    }
}

# Create Management network interface for Routers
resource "azurerm_network_interface" "router_nic_1" {
    count								= "${length(var.location)}"
    name								= "Router_NIC1-${1+count.index}"
    location							= "${element(var.location, count.index)}"
    resource_group_name					= "${element(azurerm_resource_group.versa_rg.*.name, count.index)}"
    network_security_group_id			= "${element(azurerm_network_security_group.versa_nsg.*.id, count.index)}"

    ip_configuration {
        name							= "Router_NIC1_Configuration-${1+count.index}"
		subnet_id						= "${element(azurerm_subnet.mgmt_subnet.*.id, count.index)}"
        private_ip_address_allocation	= "dynamic"
		public_ip_address_id			= "${element(azurerm_public_ip.ip_router.*.id, count.index)}"
    }

    tags {
        environment = "VersaHeadEndHA"
    }
}

# Create Northbound network interface for Routers
resource "azurerm_network_interface" "router_nic_2" {
    count								= "${length(var.location)}"
    name								= "Router_NIC2-${1+count.index}"
    location							= "${element(var.location, count.index)}"
    resource_group_name					= "${element(azurerm_resource_group.versa_rg.*.name, count.index)}"
    network_security_group_id			= "${element(azurerm_network_security_group.versa_nsg.*.id, count.index)}"
	enable_ip_forwarding				= "true"

    ip_configuration {
        name							= "Router_NIC2_Configuration-${1+count.index}"
		subnet_id						= "${element(azurerm_subnet.dir_router_network_subnet.*.id, count.index)}"
        private_ip_address_allocation	= "dynamic"
    }

    tags {
        environment = "VersaHeadEndHA"
    }
}

# Create BGP network interface for Routers (Router to Router connectivity)
resource "azurerm_network_interface" "router_nic_3" {
    count								= "${length(var.location)}"
    name								= "Router_NIC3-${1+count.index}"
    location							= "${element(var.location, count.index)}"
    resource_group_name					= "${element(azurerm_resource_group.versa_rg.*.name, count.index)}"
    network_security_group_id			= "${element(azurerm_network_security_group.versa_nsg.*.id, count.index)}"
	enable_ip_forwarding				= "true"

    ip_configuration {
        name							= "Router_NIC3_Configuration-${1+count.index}"
		subnet_id						= "${element(azurerm_subnet.router_network_subnet.*.id, count.index)}"
        private_ip_address_allocation	= "dynamic"
    }

    tags {
        environment = "VersaHeadEndHA"
    }
}

# Create Southbound network interface for Routers
resource "azurerm_network_interface" "router_nic_4" {
    count								= "${length(var.location)}"
    name								= "Router_NIC4-${1+count.index}"
    location							= "${element(var.location, count.index)}"
    resource_group_name					= "${element(azurerm_resource_group.versa_rg.*.name, count.index)}"
    network_security_group_id			= "${element(azurerm_network_security_group.versa_nsg.*.id, count.index)}"
	enable_ip_forwarding				= "true"

    ip_configuration {
        name							= "Router_NIC4_Configuration-${1+count.index}"
		subnet_id						= "${element(azurerm_subnet.control_network_subnet.*.id, count.index)}"
        private_ip_address_allocation	= "dynamic"
    }

    tags {
        environment = "VersaHeadEndHA"
    }
}

# Create Management network interface for Controllers
resource "azurerm_network_interface" "controller_nic_1" {
    count								= "${length(var.location)}"
    name								= "Controller_NIC1-${1+count.index}"
    location							= "${element(var.location, count.index)}"
    resource_group_name					= "${element(azurerm_resource_group.versa_rg.*.name, count.index)}"
    network_security_group_id			= "${element(azurerm_network_security_group.versa_nsg.*.id, count.index)}"

    ip_configuration {
        name							= "Controller_NIC1_Configuration-${1+count.index}"
		subnet_id						= "${element(azurerm_subnet.mgmt_subnet.*.id, count.index)}"
        private_ip_address_allocation	= "dynamic"
		public_ip_address_id			= "${element(azurerm_public_ip.ip_ctrl.*.id, count.index)}"
    }

    tags {
        environment = "VersaHeadEndHA"
    }
}

# Create Northbound/Control network interface for Controllers
resource "azurerm_network_interface" "controller_nic_2" {
    count								= "${length(var.location)}"
    name								= "Controller_NIC2-${1+count.index}"
    location							= "${element(var.location, count.index)}"
    resource_group_name					= "${element(azurerm_resource_group.versa_rg.*.name, count.index)}"
    network_security_group_id			= "${element(azurerm_network_security_group.versa_nsg.*.id, count.index)}"
	enable_ip_forwarding				= "true"

    ip_configuration {
        name							= "Controller_NIC2_Configuration-${1+count.index}"
		subnet_id						= "${element(azurerm_subnet.control_network_subnet.*.id, count.index)}"
        private_ip_address_allocation	= "dynamic"
    }

    tags {
        environment = "VersaHeadEndHA"
    }
}

# Create Southbound/WAN network interface for Controllers
resource "azurerm_network_interface" "controller_nic_3" {
    count								= "${length(var.location)}"
    name								= "Controller_NIC3-${1+count.index}"
    location							= "${element(var.location, count.index)}"
    resource_group_name					= "${element(azurerm_resource_group.versa_rg.*.name, count.index)}"
    network_security_group_id			= "${element(azurerm_network_security_group.versa_nsg.*.id, count.index)}"

    ip_configuration {
        name							= "Controller_NIC3_Configuration-${1+count.index}"
		subnet_id						= "${element(azurerm_subnet.wan_network_subnet.*.id, count.index)}"
        private_ip_address_allocation	= "dynamic"
		public_ip_address_id			= "${element(azurerm_public_ip.ip_ctrl_wan.*.id, count.index)}"
    }

    tags {
        environment = "VersaHeadEndHA"
    }
}

# Create Management network interface for Analytics
resource "azurerm_network_interface" "van_nic_1" {
    count								= "${length(var.location)}"
    name								= "VAN_NIC1-${1+count.index}"
    location							= "${element(var.location, count.index)}"
    resource_group_name					= "${element(azurerm_resource_group.versa_rg.*.name, count.index)}"
    network_security_group_id			= "${element(azurerm_network_security_group.versa_nsg.*.id, count.index)}"

    ip_configuration {
        name							= "VAN_NIC1_Configuration-${1+count.index}"
		subnet_id						= "${element(azurerm_subnet.mgmt_subnet.*.id, count.index)}"
        private_ip_address_allocation	= "dynamic"
		public_ip_address_id			= "${element(azurerm_public_ip.ip_van.*.id, count.index)}"
    }

    tags {
        environment = "VersaHeadEndHA"
    }
}

# Create Southbound network interface for Analytics
resource "azurerm_network_interface" "van_nic_2" {
    count								= "${length(var.location)}"
    name								= "VAN_NIC2-${1+count.index}"
    location							= "${element(var.location, count.index)}"
    resource_group_name					= "${element(azurerm_resource_group.versa_rg.*.name, count.index)}"
    network_security_group_id			= "${element(azurerm_network_security_group.versa_nsg.*.id, count.index)}"

    ip_configuration {
        name							= "VAN_NIC2_Configuration-${1+count.index}"
		subnet_id						= "${element(azurerm_subnet.dir_router_network_subnet.*.id, count.index)}"
        private_ip_address_allocation	= "dynamic"
    }

    tags {
        environment = "VersaHeadEndHA"
    }
}

# Enable global peering between the two virtual network 
resource "azurerm_virtual_network_peering" "peering" {
    count							= "${length(var.location)}"
    name							= "VNetPeering-to-${element(azurerm_virtual_network.versaNetwork.*.name, 1 - count.index)}"
    resource_group_name				= "${element(azurerm_resource_group.versa_rg.*.name, count.index)}"
    virtual_network_name			= "${element(azurerm_virtual_network.versaNetwork.*.name, count.index)}"
    remote_virtual_network_id		= "${element(azurerm_virtual_network.versaNetwork.*.id, 1 - count.index)}"
    allow_virtual_network_access	= true
    allow_forwarded_traffic			= true
    allow_gateway_transit			= false
	depends_on						= ["azurerm_virtual_machine.directorVM", "azurerm_virtual_machine.routerVM", "azurerm_virtual_machine.controllerVM", "azurerm_virtual_machine.vanVM"]
}

# Create storage account for boot diagnostics of Director VMs
resource "azurerm_storage_account" "storageaccountDir" {
    count						= "${length(var.location)}"
    name						= "dirdiag${1+count.index}"
    resource_group_name			= "${element(azurerm_resource_group.versa_rg.*.name, count.index)}"
    location					= "${element(var.location, count.index)}"
    account_tier				= "Standard"
    account_replication_type	= "LRS"

    tags {
        environment = "VersaHeadEndHA"
    }
}

# Create storage account for boot diagnostics of Router VMs
resource "azurerm_storage_account" "storageaccountRouter" {
    count						= "${length(var.location)}"
    name                        = "routdiag${1+count.index}"
    resource_group_name			= "${element(azurerm_resource_group.versa_rg.*.name, count.index)}"
    location					= "${element(var.location, count.index)}"
    account_tier                = "Standard"
    account_replication_type    = "LRS"

    tags {
        environment = "VersaHeadEndHA"
    }
}

# Create storage account for boot diagnostics of Controller VMs
resource "azurerm_storage_account" "storageaccountCtrl" {
    count						= "${length(var.location)}"
    name                        = "ctrldiag${1+count.index}"
    resource_group_name			= "${element(azurerm_resource_group.versa_rg.*.name, count.index)}"
    location					= "${element(var.location, count.index)}"
    account_tier                = "Standard"
    account_replication_type    = "LRS"

    tags {
        environment = "VersaHeadEndHA"
    }
}

# Create storage account for boot diagnostics of Analytics VMs
resource "azurerm_storage_account" "storageaccountVAN" {
    count						= "${length(var.location)}"
    name                        = "vandiag${1+count.index}"
    resource_group_name			= "${element(azurerm_resource_group.versa_rg.*.name, count.index)}"
    location					= "${element(var.location, count.index)}"
    account_tier                = "Standard"
    account_replication_type    = "LRS"

    tags {
        environment = "VersaHeadEndHA"
    }
}

# Create Versa Director Virtual Machine
resource "azurerm_virtual_machine" "directorVM" {
    count							= "${length(var.location)}"
    name                  			= "Versa_Director-${1+count.index}"
    location						= "${element(var.location, count.index)}"
    resource_group_name				= "${element(azurerm_resource_group.versa_rg.*.name, count.index)}"
    network_interface_ids 			= ["${element(azurerm_network_interface.director_nic_1.*.id, count.index)}", "${element(azurerm_network_interface.director_nic_2.*.id, count.index)}"]
	primary_network_interface_id	= "${element(azurerm_network_interface.director_nic_1.*.id, count.index)}"
    vm_size               			= "${var.director_vm_size}"

    storage_os_disk {
        name              = "Director_OSDisk-${1+count.index}"
        caching           = "ReadWrite"
        create_option     = "FromImage"
        managed_disk_type = "Standard_LRS"
    }

    storage_image_reference {
        id="${element(var.image_director, count.index)}"
    }

    os_profile {
        computer_name  = "${element(var.hostname_director, count.index)}"
        admin_username = "versa_devops"
        custom_data = "${element(data.template_file.user_data_dir.*.rendered, count.index)}"
    }

    os_profile_linux_config {
        disable_password_authentication = true
        ssh_keys {
            path     = "/home/versa_devops/.ssh/authorized_keys"
            key_data = "${var.ssh_key}"
        }
    }

    boot_diagnostics {
        enabled = "true"
        storage_uri = "${element(azurerm_storage_account.storageaccountDir.*.primary_blob_endpoint, count.index)}"
    }
	
    tags {
        environment = "VersaHeadEndHA"
    }
}

# Create Versa Router FlexVNF Machines
resource "azurerm_virtual_machine" "routerVM" {
    count							= "${length(var.location)}"
    name							= "Versa_Router-${1+count.index}"
    location						= "${element(var.location, count.index)}"
    resource_group_name				= "${element(azurerm_resource_group.versa_rg.*.name, count.index)}"
    network_interface_ids			= ["${element(azurerm_network_interface.router_nic_1.*.id, count.index)}", "${element(azurerm_network_interface.router_nic_2.*.id, count.index)}", "${element(azurerm_network_interface.router_nic_3.*.id, count.index)}", "${element(azurerm_network_interface.router_nic_4.*.id, count.index)}"]
	primary_network_interface_id	= "${element(azurerm_network_interface.router_nic_1.*.id, count.index)}"
    vm_size							= "${var.router_vm_size}"
	
    storage_os_disk {
        name              = "Router_OSDisk-${1+count.index}"
        caching           = "ReadWrite"
        create_option     = "FromImage"
        managed_disk_type = "Standard_LRS"
    }

    storage_image_reference {
        id="${element(var.image_controller, count.index)}"
    }

    os_profile {
        computer_name  = "versa-flexvnf"
        admin_username = "versa_devops"
        custom_data = "${data.template_file.user_data_ctrl.rendered}"
    }

    os_profile_linux_config {
        disable_password_authentication = true
        ssh_keys {
            path     = "/home/versa_devops/.ssh/authorized_keys"
            key_data = "${var.ssh_key}"
        }
    }

    boot_diagnostics {
        enabled = "true"
        storage_uri = "${element(azurerm_storage_account.storageaccountRouter.*.primary_blob_endpoint, count.index)}"
    }
	
    tags {
        environment = "VersaHeadEndHA"
    }
}

# Create Versa Controller Virtual Machines
resource "azurerm_virtual_machine" "controllerVM" {
    count							= "${length(var.location)}"
    name							= "Versa_Controller-${1+count.index}"
    location						= "${element(var.location, count.index)}"
    resource_group_name				= "${element(azurerm_resource_group.versa_rg.*.name, count.index)}"
    network_interface_ids			= ["${element(azurerm_network_interface.controller_nic_1.*.id, count.index)}", "${element(azurerm_network_interface.controller_nic_2.*.id, count.index)}", "${element(azurerm_network_interface.controller_nic_3.*.id, count.index)}"]
	primary_network_interface_id	= "${element(azurerm_network_interface.controller_nic_1.*.id, count.index)}"
    vm_size							= "${var.controller_vm_size}"
	
    storage_os_disk {
        name              = "Controller_OSDisk-${1+count.index}"
        caching           = "ReadWrite"
        create_option     = "FromImage"
        managed_disk_type = "Standard_LRS"
    }

    storage_image_reference {
        id="${element(var.image_controller, count.index)}"
    }

    os_profile {
        computer_name  = "versa-flexvnf"
        admin_username = "versa_devops"
        custom_data = "${data.template_file.user_data_ctrl.rendered}"
    }

    os_profile_linux_config {
        disable_password_authentication = true
        ssh_keys {
            path     = "/home/versa_devops/.ssh/authorized_keys"
            key_data = "${var.ssh_key}"
        }
    }

    boot_diagnostics {
        enabled = "true"
        storage_uri = "${element(azurerm_storage_account.storageaccountCtrl.*.primary_blob_endpoint, count.index)}"
    }
	
    tags {
        environment = "VersaHeadEndHA"
    }
}

# Create Versa Analytics Virtual Machines
resource "azurerm_virtual_machine" "vanVM" {
    count							= "${length(var.location)}"
    name							= "Versa_Analytics-${1+count.index}"
    location						= "${element(var.location, count.index)}"
    resource_group_name				= "${element(azurerm_resource_group.versa_rg.*.name, count.index)}"
    network_interface_ids			= ["${element(azurerm_network_interface.van_nic_1.*.id, count.index)}", "${element(azurerm_network_interface.van_nic_2.*.id, count.index)}"]
	primary_network_interface_id	= "${element(azurerm_network_interface.van_nic_1.*.id, count.index)}"
    vm_size							= "${var.analytics_vm_size}"

    storage_os_disk {
        name              = "VAN_OSDisk-${1+count.index}"
        caching           = "ReadWrite"
        create_option     = "FromImage"
        managed_disk_type = "Standard_LRS"
    }

    storage_image_reference {
        id="${element(var.image_analytics, count.index)}"
    }

    os_profile {
        computer_name  = "${element(var.hostname_van, count.index)}"
        admin_username = "versa_devops"
        custom_data = "${element(data.template_file.user_data_van.*.rendered, count.index)}"
    }

    os_profile_linux_config {
        disable_password_authentication = true
        ssh_keys {
            path     = "/home/versa_devops/.ssh/authorized_keys"
            key_data = "${var.ssh_key}"
        }
    }

    boot_diagnostics {
        enabled = "true"
        storage_uri = "${element(azurerm_storage_account.storageaccountVAN.*.primary_blob_endpoint, count.index)}"
    }
	
    tags {
        environment = "VersaHeadEndHA"
    }
}

data "azurerm_public_ip" "dir_pub_ip" {
    count					= "${length(var.location)}"
    name					= "${element(azurerm_public_ip.ip_dir.*.name, count.index)}"
    resource_group_name		= "${element(azurerm_resource_group.versa_rg.*.name, count.index)}"
    depends_on				= ["azurerm_virtual_machine.directorVM"]
}

data "azurerm_public_ip" "router_pub_ip" {
    count					= "${length(var.location)}"
    name					= "${element(azurerm_public_ip.ip_router.*.name, count.index)}"
    resource_group_name		= "${element(azurerm_resource_group.versa_rg.*.name, count.index)}"
    depends_on				= ["azurerm_virtual_machine.routerVM"]
}

data "azurerm_public_ip" "ctrl_pub_ip" {
    count					= "${length(var.location)}"
    name					= "${element(azurerm_public_ip.ip_ctrl.*.name, count.index)}"
    resource_group_name		= "${element(azurerm_resource_group.versa_rg.*.name, count.index)}"
    depends_on				= ["azurerm_virtual_machine.controllerVM"]
}

data "azurerm_public_ip" "van_pub_ip" {
    count					= "${length(var.location)}"
    name					= "${element(azurerm_public_ip.ip_van.*.name, count.index)}"
    resource_group_name		= "${element(azurerm_resource_group.versa_rg.*.name, count.index)}"
    depends_on				= ["azurerm_virtual_machine.vanVM"]
}

data "azurerm_public_ip" "ctrl_wan_pub_ip" {
    count					= "${length(var.location)}"
    name					= "${element(azurerm_public_ip.ip_ctrl_wan.*.name, count.index)}"
    resource_group_name		= "${element(azurerm_resource_group.versa_rg.*.name, count.index)}"
    depends_on				= ["azurerm_virtual_machine.controllerVM"]
}